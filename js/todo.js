
// Global key and token values

const key = '8e88c7a234c5ae0df7a0ca91a6894d69';
const token = '6eb43811bec9dab09bea76cc7327617fe3a990858ebf9e11962cd717165e37f5';
const trelloLink = `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`;

// Getting all cards from trello-todo-board

fetch(trelloLink)
  .then((response) => {
    return response.json();
  })
  .then(allBoards => allBoards[1].id)
  .then((board) => {
    return fetch(`https://api.trello.com/1/boards/${board}/cards`).then(response => response.json())
  })
  .then((cards) => { // Getting cards
    let checkLists = cards.map((card) => {
      return (card.idChecklists); //Getting every id of checklistss
    }).reduce((acc, val) => {
      return acc.concat(val); //Flatten the array of checklists
    }, []);
    return checkLists;
  })
  .then((checkLists) => { // Getting all checklists present in card
    let checkListItems = checkLists.map((checkList) => {
      return fetch(`https://api.trello.com/1/checklists/${checkList}?fields=all`).then(response => response.json())
      .then((items) => {
        let item = items.checkItems.map((item) => {
          return item;
        });
        return item;
      }).catch(err => console.log(err));
    });
    return Promise.all(checkListItems).then((values) => {
      return values.reduce((acc, val) => {
        return acc.concat(val);
      }, []);
    });
  })
  .then((arrayOfChecklists) => { // Getting checklist items
    controllers.appendIncompleteChecklists(arrayOfChecklists);
     // JQuery for button clicks
     $("button").click(function () {
      $(this).prev().css("text-decoration", "line-through");
      let checkId = $(this).attr('id');
      let checkItem = arrayOfChecklists.filter(checkitem => checkitem.id == checkId)[0];
      // Sending a put request
      fetch(trelloLink)
        .then(response => response.json())
        .then((allBoards) => {
          let boardRequired = allBoards[1].id;
          return boardRequired;
        })
        .then((board) => {
          return fetch(`https://api.trello.com/1/boards/${board}/cards`).then(response => response.json())
          .then((cards) => {
             let cardId = cards.filter((card) => {
              if (card.idChecklists.indexOf(checkItem.idChecklist) != -1) {
                return card.id;
              }
             });
             return cardId;
          }).catch(err => console.log(err));
        })
        .then((cardId) => {
          // Manipulating state of check items
          let request;
          if(checkItem.state == 'incomplete') {
            checkItem.state = 'complete';
            $(this).text('Undo');
            request = fetch(`https://api.trello.com/1/cards/${cardId[0].id}/checkItem/${checkItem.id}?key=${key}&token=${token}&state=complete`, {
            method: 'PUT',
             });
          } else {
            checkItem.state = 'incomplete'
            $(this).text('Done');
            $(this).prev().css("text-decoration", "none");
            request = fetch(`https://api.trello.com/1/cards/${cardId[0].id}/checkItem/${checkItem.id}?key=${key}&token=${token}&state=incomplete`, {
            method: 'PUT',
           });
          }
          fetch(request).catch(err => console.log(err));
        }).catch(err => console.log(err));
    });
  }).catch(err => console.log(err));


// Functions for updating the dom
let controllers = {
  appendIncompleteChecklists: function (checkListItems) {
    checkListItems.forEach((checklist) => {
      if (checklist.state == 'incomplete') {
        $(".list-group").append(`<div class="row">
        <p class="list-group-item list-group-item-secondary col-md-11">${checklist.name}</p>
        <button id=${checklist.id} class="btn btn-dark col-md-1" type="button">Done</button>
        </div>`);
      }
    });
  },
};